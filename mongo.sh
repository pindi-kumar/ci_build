#! /bin/bash
echo " start build"


mvn package ;

exitstatus=$?

if [ $exitstatus == 0 ] ;
  then 
    echo " send the details to mongodb"

      nbr1=$BUILD_TAG
      nbr2=$NODE_NAME
      nbr3=$WORKSPACE
      nbr4=$GIT_COMMIT 
      nbr5=$GIT_BRANCH

      mongo  --host="13.126.34.139:27017"  build   <<EOF
      var build_tag ="$nbr1" ;
      var node_name ="$nbr2" ;
      var workspace ="$nbr3" ;
      var git_commit ="$nbr4" ;
      var git_branch ="$nbr5" ;
      db.ci_build.insert(
        {
                BUILD_TAG: build_tag,
                NODE_NAME: node_name,
                WORKSPACE: workspace,
                GIT_COMMIT: git_commit,
                GIT_BRANCH: git_branch
        }
       );
           db.ci_build.find().pretty()
EOF
 
  else
    echo " "
    echo " build failure##########*****************"
fi
